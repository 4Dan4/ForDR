//
//  ViewControllerVM.swift
//  ForDR
//
//  Created by dan4 on 01.12.2022.
//

import UIKit

struct ViewControllerVM {
    
    // MARK: - Appearance
    
    var appearance: ViewControllerAppearancable = ViewControllerAppearance()
    
    // MARK: - Variables
    
    let buyButtonTitle: String
    let sellButtonTitle: String
    let spreadTitle: String
    let spreadValueText: String
    let priceTitle: String
    let quantitiTitle: String
    let settingsLabel: String
    let confirmButtonTitle: String
    
}
