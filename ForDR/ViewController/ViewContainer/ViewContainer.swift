//
//  ViewContainer.swift
//  ForDR
//
//  Created by dan4 on 01.12.2022.
//

import Foundation
import RxCocoa
import RxSwift
import SnapKit

/*
 ********************************************************
 DESCRIPTION:
 CREATED FOR:
 ********************************************************
 */

class ViewContainer: UIView, ButtonDelegate {
    // MARK: - Variables
    
    private let vr = ViewResources()
    var viewModel: ViewControllerVM?
    var buyButtonIsActive: Bool?
    
    // MARK: - Constructors
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initView()
        initConstraints()
        initActions()
        initConfig()
        vr.buyButton.delegate = self
        vr.sellButton.delegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /// View Initialization.
    ///
    /// Here the views are registered, their states and location in the
    /// paternal views are set.
    ///
    private func initView() {
        self.addSubview(vr.stackView)
        [vr.priceTextField, vr.quantitiTextField, vr.settingsView].forEach { vr.stackView.addArrangedSubview($0) }
        [vr.buyButton, vr.sellButton, vr.spreadView, vr.confirmButton].forEach { self.addSubview($0) }
        [vr.settingsLabel, vr.moreButton].forEach { vr.settingsView.addSubview($0) }
        notificationHandler()
    }
    
    func initConfig() {
        vr.buyButton.configure(with: BuyButtonVMBuilder().vm)
        vr.sellButton.configure(with: SellButtonVMBuilder().vm)
        vr.priceTextField.configure(with: TextFieldWithTitleVMBuilder().vm)
        vr.quantitiTextField.configure(with: TextFieldWithTitleVMBuilder().vmWithoutClearTitle)
        vr.spreadView.configure(with: SpreadViewVMBuilder().vm)
    }
    
    /// Constraints initialization
    ///
    /// Setup constraints for controls are set
    ///
    private func initConstraints() {
        vr.buyButton.snp.makeConstraints {
            $0.top.equalToSuperview().inset(50)
            $0.leading.equalToSuperview().inset(17)
            $0.width.equalTo(self.frame.width/2-17)
            $0.height.equalTo(64)
        }
        vr.sellButton.snp.makeConstraints {
            $0.top.equalToSuperview().inset(50)
            $0.trailing.equalToSuperview().inset(17)
            $0.width.equalTo(self.frame.width/2-17)
            $0.height.equalTo(64)
        }
        vr.spreadView.snp.makeConstraints {
            $0.top.equalTo(vr.buyButton.snp.top).inset(7)
            $0.centerX.equalTo(self.center.x)
        }
        vr.stackView.snp.makeConstraints{
            $0.top.equalTo(vr.buyButton.snp.bottom).offset(19)
            $0.leading.trailing.equalToSuperview().inset(17)
        }
        vr.priceTextField.snp.makeConstraints {
            $0.top.equalToSuperview().inset(0)
            $0.height.equalTo(75)
        }
        vr.quantitiTextField.snp.makeConstraints {
            $0.top.equalTo(vr.priceTextField.snp.bottom).offset(27)
            $0.height.equalTo(75)
        }
        vr.settingsView.snp.makeConstraints {
            $0.top.equalTo(vr.quantitiTextField.snp.bottom).offset(33)
            $0.height.equalTo(58)
        }
        vr.settingsLabel.snp.makeConstraints {
            $0.top.bottom.equalToSuperview().inset(13)
            $0.leading.equalToSuperview().inset(16)
        }
        vr.moreButton.snp.makeConstraints {
            $0.top.bottom.equalToSuperview().inset(12)
            $0.trailing.equalToSuperview().inset(17)
        }
        vr.confirmButton.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(16)
            $0.bottom.equalToSuperview().inset(80)
            $0.height.equalTo(52)
        }
    }
    
}

// MARK: - Actions

/*
 *********************** Actions ************************
 Handling user actions. For example, clicking on a button,
 changing the segment index
 ********************************************************
 */

extension ViewContainer {
    
    private func initActions() {
        vr.confirmButton.addTarget(self, action: #selector(confirmButtonAction), for: .touchUpInside)
        vr.moreButton.addTarget(self, action: #selector(moreButtonAction), for: .touchUpInside)
    }
    
    private func notificationHandler() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.keyboardWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil)
    }
    
    @objc
    private func buyButtonAction() {
        print("***")
        print("Buy")
        print("***")
        checkBuyButtonState(with: ViewControllerVMBuilder().vm)
    }
    
    @objc
    private func sellButtonAction() {
        print("****")
        print("Cell")
        print("****")
        checkSellButtonState(with: ViewControllerVMBuilder().vm)
    }
    
    @objc
    private func moreButtonAction() {
        print("****")
        print("More")
        print("****")
    }
    
    @objc
    private func confirmButtonAction() {
        UIView.animate(withDuration: 0.9) {
            if self.vr.quantitiTextField.isHidden == true {
                self.vr.quantitiTextField.isHidden = false
            }
            if self.vr.priceTextField.isHidden == true {
                self.vr.priceTextField.isHidden = false
            }
        }
    }
    
    @objc
    func keyboardWillShow(notification: NSNotification) {
        var kbHeight = 0
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            kbHeight = Int(keyboardRectangle.height)
        }
        UIView.animate(withDuration: 0.9) {
            self.vr.confirmButton.snp.remakeConstraints {
                $0.bottom.equalToSuperview().inset(kbHeight + 16)
                $0.leading.trailing.equalToSuperview().inset(16)
                $0.height.equalTo(52)
            }
        }
    }
    
    @objc
    func keyboardWillHide(notification: NSNotification) {
        UIView.animate(withDuration: 0.9) {
            self.vr.confirmButton.snp.remakeConstraints {
                $0.bottom.equalToSuperview().inset(80)
                $0.leading.trailing.equalToSuperview().inset(16)
                $0.height.equalTo(52)
            }
        }
    }
    
    private func checkBuyButtonState(with viewModel: ViewControllerVM) {
        self.viewModel = viewModel
        if vr.priceTextField.isHidden != true && vr.quantitiTextField.isHidden != true {
            vr.sellButton.backgroundColor = viewModel.appearance.sellButtonInactiveColor
            vr.buyButton.backgroundColor = viewModel.appearance.buyButtonActiveColor
            vr.priceTextField.isHidden = true
            vr.confirmButton.setTitle("buy", for: .normal)
        }
         else {
            print("Some views is already hiden")
        }
    }
    
    private func checkSellButtonState(with viewModel: ViewControllerVM) {
        self.viewModel = viewModel
        if vr.priceTextField.isHidden != true && vr.quantitiTextField.isHidden != true {
            vr.buyButton.backgroundColor = viewModel.appearance.buyButtonInactiveColor
            vr.sellButton.backgroundColor = viewModel.appearance.sellButtonActiveColor
            vr.quantitiTextField.isHidden = true
            vr.confirmButton.setTitle("sell", for: .normal)
        } else {
            print("Some views is already hiden")
        }
    }
    
}

// MARK: - Configuration

/*
 ******************** Configuration *********************
 All control configuration logic, based on ViewModel.
 Such as: establishing the connection of the view model with
 the state machine, setting the data of all internal components,
 setting the color palette, fonts, pictures.
 ********************************************************
 */

extension ViewContainer {

    /// Configuration of control
    ///
    /// Linking the view model with the state machine, appearance
    /// and setting data for inner controls
    ///
    /// - parameter viewModel: ViewModel with necessary data
    ///
    func configure(with viewModel: ViewControllerVM) {
        self.viewModel = viewModel
        setupData(viewModel)
        setupAppearance(viewModel.appearance)
    }
    
    /// Setup data for inner controls
    ///
    /// - parameter viewModel: ViewModel with data for inner controls
    ///
    private func setupData(_ data: ViewControllerVM) {
        vr.settingsLabel.text = data.settingsLabel
        vr.confirmButton.setTitle(data.confirmButtonTitle, for: .normal)
    }
    
    /// Setup appearance for current control
    ///
    /// - parameter appearance: Appearancable class with color, font, image settings
    ///
    private func setupAppearance(_ appearance: ViewControllerAppearancable) {
        self.backgroundColor = appearance.backgroundColor
        vr.buyButton.backgroundColor = appearance.buyButtonActiveColor
        vr.buyButton.layer.cornerRadius = 8
        vr.buyButton.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
        vr.sellButton.backgroundColor = appearance.sellButtonInactiveColor
        vr.sellButton.layer.cornerRadius = 8
        vr.sellButton.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
        vr.settingsLabel.textColor = appearance.settingsLabelColor
        vr.settingsLabel.font = appearance.settingsLabelFont
        vr.confirmButton.setTitleColor(appearance.confirmButtonTitleColor, for: .normal)
        vr.buyButton.tintColor = appearance.buySellTintColor
        vr.sellButton.tintColor = appearance.buySellTintColor
        vr.settingsView.backgroundColor = appearance.settingsViewBgc
        vr.moreButton.tintColor = appearance.moreButtonTintColor
        vr.confirmButton.backgroundColor = appearance.confirmButtonBgc
        vr.confirmButton.tintColor = appearance.confirmButtonTintColor
        vr.spreadView.layer.cornerRadius = 8
        vr.spreadView.layer.borderWidth = 2
        vr.spreadView.layer.borderColor = appearance.spreadViewBorderColor.cgColor
    }
    
}

// MARK: - ViewResources

/*
 ******************** View Resources ********************
 All internal components are hidden in resources, for
 readability of the code, and, if necessary, reuse resources in
 other components.
 ********************************************************
 */

private class ViewResources {
    lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 27
        return stackView
    }()
    
    lazy var buyButton = ButtonView()
    lazy var sellButton = ButtonView()
    lazy var priceTextField = TextfieldWithTitleView()
    lazy var quantitiTextField = TextfieldWithTitleView()
    lazy var spreadView = SpreadView()
    
    lazy var settingsView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 8
        return view
    }()
    
    lazy var settingsLabel: UILabel = {
        let label = UILabel()
        return label
    }()
    
    lazy var moreButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "more"), for: .normal)
        return button
    }()
    
    lazy var confirmButton: UIButton = {
        let button = UIButton(type: .system)
        button.layer.cornerRadius = 8
        return button
    }()
}

extension ViewContainer: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        vr.priceTextField.resignFirstResponder()
        vr.quantitiTextField.resignFirstResponder()
        return true
    }
}

extension ViewContainer {
    func buttonDelegate(isActive: Bool) {
        if isActive == true {
            buyButtonAction()
        } else if isActive == false {
            sellButtonAction()
        }
    }
}
