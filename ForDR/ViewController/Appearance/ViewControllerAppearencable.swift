//
//  ViewControllerAppearencable.swift
//  ForDR
//
//  Created by dan4 on 01.12.2022.
//

import UIKit

protocol ViewControllerAppearancable {
    var backgroundColor: UIColor { get }
    var buyButtonActiveColor: UIColor { get }
    var buyButtonInactiveColor: UIColor { get }
    var sellButtonActiveColor: UIColor { get }
    var sellButtonInactiveColor: UIColor { get }
    var buySellTintColor: UIColor { get }
    var spreadLabelColor: UIColor { get }
    var spreadLabelFont: UIFont { get }
    var spreadValueColor: UIColor { get }
    var spreadValueFont: UIFont { get }
    var settingsLabelColor: UIColor { get }
    var settingsLabelFont: UIFont { get }
    var confirmButtonTitleColor: UIColor { get }
    var settingsViewBgc: UIColor { get }
    var moreButtonTintColor: UIColor { get }
    var confirmButtonBgc: UIColor { get }
    var confirmButtonTintColor: UIColor { get }
    var spreadViewBorderColor: UIColor { get }
}

class ViewControllerAppearance: ViewControllerAppearancable {
    var backgroundColor: UIColor { TW.gray900 }
    var buyButtonActiveColor: UIColor { TW.green500 }
    var buyButtonInactiveColor: UIColor { TW.gray800 }
    var sellButtonActiveColor: UIColor { TW.red600 }
    var sellButtonInactiveColor: UIColor { TW.gray800 }
    var buySellTintColor: UIColor { TW.white }
    var spreadLabelColor: UIColor { TW.gray400 }
    var spreadLabelFont: UIFont { .systemFont(ofSize: 12) }
    var spreadValueColor: UIColor { TW.gray400 }
    var spreadValueFont: UIFont { .systemFont(ofSize: 12) }
    var settingsLabelColor: UIColor { TW.white }
    var settingsLabelFont: UIFont { .systemFont(ofSize: 16) }
    var confirmButtonTitleColor: UIColor { TW.gray400 }
    var settingsViewBgc: UIColor { TW.gray800 }
    var moreButtonTintColor: UIColor { TW.gray500 }
    var confirmButtonBgc: UIColor { TW.gray700 }
    var confirmButtonTintColor: UIColor { TW.gray400 }
    var spreadViewBorderColor: UIColor { TW.gray800 }
}
