//
//  ViewController.swift
//  ForDR
//
//  Created by dan4 on 26.11.2022.
//

import UIKit

final class ViewController: UIViewController {
    
    // MARK: - Main View
    
    private lazy var viewContainer = ViewContainer(frame: view.frame)
    private let vm = ViewControllerVMBuilder().vm
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewContainer.configure(with: vm)
        mainInit()
    }
    
    // MARK: - Inits
    
    private func mainInit() {
        view.addSubview(viewContainer)
        self.hideKeyboardWhenTappedAround()
    }
}
