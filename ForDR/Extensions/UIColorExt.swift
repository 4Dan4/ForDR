//
//  UIColorExt.swift
//  ForDR
//
//  Created by dan4 on 28.11.2022.
//

import UIKit

extension UIColor {
    var alpha: CGFloat {
        var cR: CGFloat = 0
        var cG: CGFloat = 0
        var cB: CGFloat = 0
        var cA: CGFloat = 0
        
        self.getRed(&cR, green: &cG, blue: &cB, alpha: &cA)
        return cA
    }
    
    /// Init color with RGB (0..255) values
    convenience init(r red: Int, g green: Int, b blue: Int, a alpha: Int = 1) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: CGFloat(alpha))
    }
    
    /// Init color with RGB (0..255) values
    convenience init(r red: CGFloat, g green: CGFloat, b blue: CGFloat, a alpha: CGFloat = 1.0) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: CGFloat(alpha))
    }
    
    /// Init with RGB encoded in Int
    convenience init(rgb: Int) {
        self.init(
            r: (rgb >> 16) & 0xFF,
            g: (rgb >> 8) & 0xFF,
            b: rgb & 0xFF
        )
    }
    
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let thisAlpha, thisRed, thisGreen, thisBlue: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (thisAlpha, thisRed, thisGreen, thisBlue) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (thisAlpha, thisRed, thisGreen, thisBlue) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (thisAlpha, thisRed, thisGreen, thisBlue) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
            
        default:
            (thisAlpha, thisRed, thisGreen, thisBlue) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(thisRed) / 255, green: CGFloat(thisGreen) / 255, blue: CGFloat(thisBlue) / 255, alpha: CGFloat(thisAlpha) / 255)
    }
}

extension UIColor {
    
    var webColor: String {
        var thisRed: CGFloat = 0
        var thisGreen: CGFloat = 0
        var thisBlue: CGFloat = 0
        var thisAlpha: CGFloat = 0
        self.getRed(&thisRed, green: &thisGreen, blue: &thisBlue, alpha: &thisAlpha)
        return "rgba(\(Int(thisRed * 255.0)), \(Int(thisGreen * 255.0)), \(Int(thisBlue * 255.0)), \(thisAlpha * 255.0))"
    }
    
    /// Hex color representation string
    var hexString: String {
        var thisRed: CGFloat = 0
        var thisGreen: CGFloat = 0
        var thisBlue: CGFloat = 0
        var thisAlpha: CGFloat = 0
        self.getRed(&thisRed, green: &thisGreen, blue: &thisBlue, alpha: &thisAlpha)
        
        var color = String(
            format: "%02lX%02lX%02lX",
            lroundf(Float(thisRed * 255.0)),
            lroundf(Float(thisGreen * 255.0)),
            lroundf(Float(thisBlue * 255.0))
        )
        
        if thisAlpha < 1 {
            color = String(format: "%02lX", lroundf(Float(thisAlpha * 255.0))) + color
        }
        
        return "#" + color
    }
    
    func mixed(with overlay: UIColor) -> UIColor {
        var bgR: CGFloat = 0
        var bgG: CGFloat = 0
        var bgB: CGFloat = 0
        var bgA: CGFloat = 0
        
        var fgR: CGFloat = 0
        var fgG: CGFloat = 0
        var fgB: CGFloat = 0
        var fgA: CGFloat = 0
        
        self.getRed(&bgR, green: &bgG, blue: &bgB, alpha: &bgA)
        overlay.getRed(&fgR, green: &fgG, blue: &fgB, alpha: &fgA)
        
        let r = fgA * fgR + (1 - fgA) * bgR
        let g = fgA * fgG + (1 - fgA) * bgG
        let b = fgA * fgB + (1 - fgA) * bgB
        
        return UIColor(red: r, green: g, blue: b, alpha: 1.0)
    }
    
    func gradient(to color: UIColor, percentage percentageDouble: Double = 0.5) -> UIColor {
        let percentage = CGFloat(percentageDouble)
        
        var (r1, g1, b1, a1): (CGFloat, CGFloat, CGFloat, CGFloat) = (0, 0, 0, 0)
        var (r2, g2, b2, a2): (CGFloat, CGFloat, CGFloat, CGFloat) = (0, 0, 0, 0)
        guard self.getRed(&r1, green: &g1, blue: &b1, alpha: &a1) else { return self }
        guard color.getRed(&r2, green: &g2, blue: &b2, alpha: &a2) else { return self }
        
        let red: CGFloat = max(0, min(255, CGFloat(min(r1, r2) + abs(r1 - r2) * percentage)))
        let green: CGFloat = max(0, min(255, CGFloat(min(g1, g2) + abs(g1 - g2) * percentage)))
        let blue: CGFloat = max(0, min(255, CGFloat(min(b1, b2) + abs(b1 - b2) * percentage)))
        let alpha: CGFloat = max(0, min(255, CGFloat(min(a1, a2) + abs(a1 - a2) * percentage)))
        
        return UIColor(red: red,
                       green: green,
                       blue: blue,
                       alpha: alpha
        )
        
    }
    
    func gradient(with color: UIColor, percentage percentageDouble: Double = 0.5) -> UIColor {
        return gradient(to: color, percentage: percentageDouble)
    }
    
}

extension UIImage {
    var pixelWidth: Int {
        return cgImage?.width ?? 0
    }
    
    var pixelHeight: Int {
        return cgImage?.height ?? 0
    }
    
    func getPixelColor(_ pos: CGPoint) -> UIColor? {
        guard let cgImage = self.cgImage else {
            return nil
        }
        guard pixelWidth > Int(pos.x), pixelHeight > Int(pos.y) else {
            return nil
        }
        guard let cgImageGetDataProvider = cgImage.dataProvider else {
            return nil
        }
        
        let pixelData = cgImageGetDataProvider.data
        let data: UnsafePointer<UInt8> = CFDataGetBytePtr(pixelData)
        
        let pixelInfo: Int = ((Int(self.size.width) * Int(pos.y)) + Int(pos.x)) * 4
        
        let r = CGFloat(data[pixelInfo]) / CGFloat(255.0)
        let g = CGFloat(data[pixelInfo + 1]) / CGFloat(255.0)
        let b = CGFloat(data[pixelInfo + 2]) / CGFloat(255.0)
        let a = CGFloat(data[pixelInfo + 3]) / CGFloat(255.0)
        
        return UIColor(red: r, green: g, blue: b, alpha: a)
    }
    
    func getPixelAlpha(_ pos: CGPoint) -> CGFloat? {
        
        return pixelColor(x: Int(pos.x), y: Int(pos.y)).alpha
    }
}

extension UIColor {
    var imageValue: UIImage? {
        let rect = CGRect(origin: .zero, size: CGSize(width: 1, height: 1))
        UIGraphicsBeginImageContext(rect.size)
        guard let context = UIGraphicsGetCurrentContext() else {
            return nil
        }
        context.setFillColor(self.cgColor)
        context.fill(rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }
}
public extension UIImage {

    func pixelColor(x: Int, y: Int) -> UIColor {
        assert(
            0..<pixelWidth ~= x && 0..<pixelHeight ~= y,
            "Pixel coordinates are out of bounds")

        guard
            let cgImage = cgImage,
            let data = cgImage.dataProvider?.data,
            let dataPtr = CFDataGetBytePtr(data),
            let colorSpaceModel = cgImage.colorSpace?.model,
            let componentLayout = cgImage.bitmapInfo.componentLayout
        else {
            assertionFailure("Could not get a pixel of an image")
            return TW.clear
        }

        assert(
            colorSpaceModel == .rgb,
            "The only supported color space model is RGB")
        guard cgImage.bitsPerPixel == 32 || cgImage.bitsPerPixel == 24 else {
            //            "A pixel is expected to be either 4 or 3 bytes in size")
            return UIColor.black
        }

        let bytesPerRow = cgImage.bytesPerRow
        let bytesPerPixel = cgImage.bitsPerPixel / 8
        let pixelOffset = y * bytesPerRow + x * bytesPerPixel

        if componentLayout.count == 4 {
            let components = (
                dataPtr[pixelOffset + 0],
                dataPtr[pixelOffset + 1],
                dataPtr[pixelOffset + 2],
                dataPtr[pixelOffset + 3]
            )

            var alpha: UInt8 = 0
            var red: UInt8 = 0
            var green: UInt8 = 0
            var blue: UInt8 = 0

            switch componentLayout {
            case .bgra:
                alpha = components.3
                red = components.2
                green = components.1
                blue = components.0
            case .abgr:
                alpha = components.0
                red = components.3
                green = components.2
                blue = components.1
            case .argb:
                alpha = components.0
                red = components.1
                green = components.2
                blue = components.3
            case .rgba:
                alpha = components.3
                red = components.0
                green = components.1
                blue = components.2
            default:
                return TW.clear
            }

            // If chroma components are premultiplied by alpha and the alpha is `0`,
            // keep the chroma components to their current values.
            if cgImage.bitmapInfo.chromaIsPremultipliedByAlpha && alpha != 0 {
                let invUnitAlpha = 255 / CGFloat(alpha)
                red = UInt8((CGFloat(red) * invUnitAlpha).rounded())
                green = UInt8((CGFloat(green) * invUnitAlpha).rounded())
                blue = UInt8((CGFloat(blue) * invUnitAlpha).rounded())
            }

            return .init(red: red, green: green, blue: blue, alpha: alpha)

        } else if componentLayout.count == 3 {
            let components = (
                dataPtr[pixelOffset + 0],
                dataPtr[pixelOffset + 1],
                dataPtr[pixelOffset + 2]
            )

            var red: UInt8 = 0
            var green: UInt8 = 0
            var blue: UInt8 = 0

            switch componentLayout {
            case .bgr:
                red = components.2
                green = components.1
                blue = components.0
            case .rgb:
                red = components.0
                green = components.1
                blue = components.2
            default:
                return TW.clear
            }

            return .init(red: red, green: green, blue: blue, alpha: UInt8(255))

        } else {
            assertionFailure("Unsupported number of pixel components")
            return TW.clear
        }
    }

}

public extension UIColor {

    convenience init(red: UInt8, green: UInt8, blue: UInt8, alpha: UInt8) {
        self.init(
            red: CGFloat(red) / 255,
            green: CGFloat(green) / 255,
            blue: CGFloat(blue) / 255,
            alpha: CGFloat(alpha) / 255)
    }

}

public extension CGBitmapInfo {

    enum ComponentLayout {

        case bgra
        case abgr
        case argb
        case rgba
        case bgr
        case rgb

        var count: Int {
            switch self {
            case .bgr, .rgb: return 3
            default: return 4
            }
        }

    }

    var componentLayout: ComponentLayout? {
        guard let alphaInfo = CGImageAlphaInfo(rawValue: rawValue & Self.alphaInfoMask.rawValue) else { return nil }
        let isLittleEndian = contains(.byteOrder32Little)

        if alphaInfo == .none {
            return isLittleEndian ? .bgr : .rgb
        }
        let alphaIsFirst = alphaInfo == .premultipliedFirst || alphaInfo == .first || alphaInfo == .noneSkipFirst

        if isLittleEndian {
            return alphaIsFirst ? .bgra : .abgr
        } else {
            return alphaIsFirst ? .argb : .rgba
        }
    }

    var chromaIsPremultipliedByAlpha: Bool {
        let alphaInfo = CGImageAlphaInfo(rawValue: rawValue & Self.alphaInfoMask.rawValue)
        return alphaInfo == .premultipliedFirst || alphaInfo == .premultipliedLast
    }

}
