//
//  FieldsControllerVMBuilder.swift
//  ForDR
//
//  Created by dan4 on 03.12.2022.
//

import UIKit

class FieldsControllerVMBuilder {
    
    let vm = FieldsControllerVM (
        titleLabel: "Main Label",
        buyButtonTitle: "Buy\n0.11",
        sellButtonTitle: "Sell\n0.11",
        spreadTitle: "Spread",
        spreadValueText: "0.13",
        settingsLabel: "Order settings",
        priceLabel: "Price",
        quantitiLabel: "Quantiti"
    )
    
}
