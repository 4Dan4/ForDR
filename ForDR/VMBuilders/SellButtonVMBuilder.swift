//
//  SellButtonVMBuilder.swift
//  ForDR
//
//  Created by dan4 on 06.12.2022.
//

import UIKit

class SellButtonVMBuilder {
    
    let vm = ButtonVM(appearance: SellButtonAppearance(), buttonTitle: "Sell\n0.1", buttonType: .sellButton)
    
}
