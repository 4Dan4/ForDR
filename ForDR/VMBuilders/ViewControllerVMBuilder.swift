//
//  ViewControllerVMBuilder.swift
//  ForDR
//
//  Created by dan4 on 01.12.2022.
//

import UIKit

class ViewControllerVMBuilder {
    
    let vm = ViewControllerVM (
        buyButtonTitle: "Buy\n0.11",
        sellButtonTitle: "Sell\n0.11",
        spreadTitle: "Spread",
        spreadValueText: "0.13",
        priceTitle: "Price",
        quantitiTitle: "Quantiti",
        settingsLabel: "Order settings",
        confirmButtonTitle: "buy"
    )
    
}
