//
//  BuyButtonVMBuilder.swift
//  ForDR
//
//  Created by dan4 on 06.12.2022.
//

import UIKit

class BuyButtonVMBuilder {
    
    let vm = ButtonVM(appearance: BuyButtonAppearance(), buttonTitle: "Buy\n0.1", buttonType: .buyButton)
    
}
