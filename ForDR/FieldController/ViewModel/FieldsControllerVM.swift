//
//  FieldsControllerVM.swift
//  ForDR
//
//  Created by dan4 on 03.12.2022.
//

import UIKit

struct FieldsControllerVM {
    
    // MARK: - Appearance
    
    var appearance: FieldsControllerAppearance = FieldsControllerAppearance()
    
    // MARK: - Variables
    
    let titleLabel: String
    let buyButtonTitle: String
    let sellButtonTitle: String
    let spreadTitle: String
    let spreadValueText: String
    let settingsLabel: String
    let priceLabel: String
    let quantitiLabel: String
    
}
