//
//  FieldsContainer.swift
//  ForDR
//
//  Created by dan4 on 02.12.2022.
//

import Foundation
import RxCocoa
import RxSwift
import SnapKit

/*
 ********************************************************
 DESCRIPTION:
 CREATED FOR:
 ********************************************************
 */

class FieldsContainer: UIView, ButtonDelegate {
    // MARK: - Variables
    
    private let vr = ViewResources()
    var viewModel: FieldsControllerVM?
    
    // MARK: - Constructors
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initView()
        initConstraints()
        initActions()
        initConfig()
        vr.buyButton.delegate = self
        vr.sellButton.delegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /// View Initialization.
    ///
    /// Here the views are registered, their states and location in the
    /// paternal views are set.
    ///
    private func initView() {
        [vr.titleLabel ,vr.buyButton, vr.sellButton, vr.spreadView, vr.stackView].forEach { self.addSubview($0) }
        [vr.priceBoldLabel, vr.priceTextField, vr.quantitiBoldLabel, vr.quantitiTextField, vr.settingsView].forEach { vr.stackView.addArrangedSubview($0) }
        [vr.settingsLabel, vr.moreButton].forEach { vr.settingsView.addSubview($0) }
    }
    
    func initConfig() {
        vr.buyButton.configure(with: BuyButtonVMBuilder().vm)
        vr.sellButton.configure(with: SellButtonVMBuilder().vm)
        vr.priceTextField.configure(with: TextFieldWithTitleVMBuilder().vm)
        vr.quantitiTextField.configure(with: TextFieldWithTitleVMBuilder().vmWithoutClearTitle)
        vr.spreadView.configure(with: SpreadViewVMBuilder().vm)
    }
    
    /// Constraints initialization
    ///
    /// Setup constraints for controls are set
    ///
    private func initConstraints() {
        vr.titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().inset(50)
            $0.leading.equalToSuperview().inset(17)
        }
        vr.buyButton.snp.makeConstraints {
            $0.top.equalTo(vr.titleLabel).inset(40)
            $0.leading.equalToSuperview().inset(17)
            $0.width.equalTo(self.frame.width/2-17)
            $0.height.equalTo(64)
        }
        vr.sellButton.snp.makeConstraints {
            $0.top.equalTo(vr.titleLabel).inset(40)
            $0.trailing.equalToSuperview().inset(17)
            $0.width.equalTo(self.frame.width/2-17)
            $0.height.equalTo(64)
        }
        vr.spreadView.snp.makeConstraints {
            $0.top.equalTo(vr.buyButton.snp.top).inset(7)
            $0.centerX.equalTo(self.center.x)
        }
        vr.stackView.snp.makeConstraints {
            $0.top.equalTo(vr.buyButton.snp.bottom).offset(19)
            $0.leading.trailing.equalToSuperview().inset(17)
        }
        vr.priceTextField.snp.makeConstraints {
            $0.top.equalTo(vr.priceBoldLabel.snp.bottom).offset(10)
            $0.leading.equalToSuperview().inset(0)
            $0.height.equalTo(75)
        }
        vr.quantitiTextField.snp.makeConstraints {
            $0.top.equalTo(vr.quantitiBoldLabel.snp.bottom).offset(10)
            $0.leading.equalToSuperview().inset(0)
            $0.height.equalTo(75)
        }
        vr.settingsView.snp.makeConstraints {
            $0.top.equalTo(vr.quantitiTextField.snp.bottom).offset(33)
            $0.leading.trailing.equalToSuperview().inset(0)
            $0.height.equalTo(58)
        }
        vr.settingsLabel.snp.makeConstraints {
            $0.top.bottom.equalToSuperview().inset(13)
            $0.leading.equalToSuperview().inset(16)
        }
        vr.moreButton.snp.makeConstraints {
            $0.top.bottom.equalToSuperview().inset(12)
            $0.trailing.equalToSuperview().inset(17)
        }
        vr.priceBoldLabel.snp.makeConstraints {
            $0.top.equalTo(vr.buyButton.snp.bottom).offset(19)
            $0.leading.equalToSuperview().inset(0)
        }
        vr.quantitiBoldLabel.snp.makeConstraints {
            $0.top.equalTo(vr.priceTextField.snp.bottom).offset(27)
            $0.leading.equalToSuperview().inset(0)
        }
    }
    
}

// MARK: - Actions

/*
 *********************** Actions ************************
 Handling user actions. For example, clicking on a button,
 changing the segment index
 ********************************************************
 */

extension FieldsContainer {
    
    private func initActions() {
        vr.moreButton.addTarget(self, action: #selector(moreButtonAction), for: .touchUpInside)
    }
    
    @objc
    private func buyButtonAction() {
        print("***")
        print("Buy")
        print("***")
        checkBuyButtonState(with: FieldsControllerVMBuilder().vm)
    }
    
    @objc
    private func sellButtonAction() {
        print("****")
        print("Cell")
        print("****")
        checkSellButtonState(with: FieldsControllerVMBuilder().vm)
    }
    
    @objc
    private func moreButtonAction() {
        print("************************")
        print("More in FieldsController")
        print("************************")
    }
    
    private func checkBuyButtonState(with viewModel: FieldsControllerVM) {
        self.viewModel = viewModel
        vr.sellButton.backgroundColor = viewModel.appearance.sellButtonInactiveColor
        vr.buyButton.backgroundColor = viewModel.appearance.buyButtonActiveColor
    }
    
    private func checkSellButtonState(with viewModel: FieldsControllerVM) {
        self.viewModel = viewModel
        vr.buyButton.backgroundColor = viewModel.appearance.buyButtonInactiveColor
        vr.sellButton.backgroundColor = viewModel.appearance.sellButtonActiveColor
    }
    
}

// MARK: - Configuration

/*
 ******************** Configuration *********************
 All control configuration logic, based on ViewModel.
 Such as: establishing the connection of the view model with
 the state machine, setting the data of all internal components,
 setting the color palette, fonts, pictures.
 ********************************************************
 */

extension FieldsContainer {

    /// Configuration of control
    ///
    /// Linking the view model with the state machine, appearance
    /// and setting data for inner controls
    ///
    /// - parameter viewModel: ViewModel with necessary data
    ///
    func configure(with viewModel: FieldsControllerVM) {
        self.viewModel = viewModel
        setupData(viewModel)
        setupAppearance(viewModel.appearance)
    }
    
    /// Setup data for inner controls
    ///
    /// - parameter viewModel: ViewModel with data for inner controls
    ///
    private func setupData(_ data: FieldsControllerVM) {
        vr.titleLabel.text = data.titleLabel
        vr.settingsLabel.text = data.settingsLabel
        vr.priceBoldLabel.text = data.priceLabel
        vr.quantitiBoldLabel.text = data.quantitiLabel
    }
    
    /// Setup appearance for current control
    ///
    /// - parameter appearance: Appearancable class with color, font, image settings
    ///
    private func setupAppearance(_ appearance: FieldsControllerAppearance) {
        self.backgroundColor = appearance.backgroundColor
        vr.titleLabel.textColor = appearance.titleLabelColor
        vr.titleLabel.font = appearance.titleLabelFont
        vr.buyButton.backgroundColor = appearance.buyButtonActiveColor
        vr.buyButton.layer.cornerRadius = 8
        vr.buyButton.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
        vr.sellButton.backgroundColor = appearance.sellButtonInactiveColor
        vr.sellButton.layer.cornerRadius = 8
        vr.sellButton.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
        vr.settingsLabel.textColor = appearance.settingsLabelColor
        vr.settingsLabel.font = appearance.settingsLabelFont
        vr.buyButton.tintColor = appearance.buySellTintColor
        vr.sellButton.tintColor = appearance.buySellTintColor
        vr.settingsView.backgroundColor = appearance.settingsViewBgc
        vr.moreButton.tintColor = appearance.moreButtonTintColor
        vr.spreadView.layer.cornerRadius = 8
        vr.spreadView.layer.borderWidth = 2
        vr.spreadView.layer.borderColor = appearance.spreadViewBorderColor.cgColor
        vr.priceBoldLabel.textColor = appearance.priceBoldLabelColor
        vr.priceBoldLabel.font = appearance.priceBoldLabelFont
        vr.quantitiBoldLabel.textColor = appearance.quantitiBoldLabelColor
        vr.quantitiBoldLabel.font = appearance.quantitiBoldLabelFont
    }
    
}

// MARK: - ViewResources

/*
 ******************** View Resources ********************
 All internal components are hidden in resources, for
 readability of the code, and, if necessary, reuse resources in
 other components.
 ********************************************************
 */

private class ViewResources {
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        return label
    }()
    
    lazy var buyButton = ButtonView()
    lazy var sellButton = ButtonView()
    
    lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 27
        return stackView
    }()
    
    lazy var priceBoldLabel: UILabel = {
        let label = UILabel()
        return label
    }()
    
    lazy var priceTextField = TextfieldWithTitleView()
    lazy var quantitiTextField = TextfieldWithTitleView()
    lazy var spreadView = SpreadView()
    
    lazy var quantitiBoldLabel: UILabel = {
        let label = UILabel()
        return label
    }()
    
    lazy var settingsView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 8
        return view
    }()
    
    lazy var settingsLabel: UILabel = {
        let label = UILabel()
        return label
    }()
    
    lazy var moreButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "more"), for: .normal)
        return button
    }()
}

extension FieldsContainer: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        vr.priceTextField.resignFirstResponder()
        vr.quantitiTextField.resignFirstResponder()
        return true
    }
}

extension FieldsContainer {
    func buttonDelegate(isActive: Bool) {
        if isActive == true {
            buyButtonAction()
        } else if isActive == false {
            sellButtonAction()
        }
    }
}
