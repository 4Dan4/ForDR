//
//  FieldsControllerAppearancable.swift
//  ForDR
//
//  Created by dan4 on 03.12.2022.
//

import UIKit

class FieldsControllerAppearance: ViewControllerAppearance{
    var titleLabelColor: UIColor { TW.white }
    var titleLabelFont: UIFont { .boldSystemFont(ofSize: 16) }
    override var backgroundColor: UIColor { TW.gray900 }
    override var buyButtonActiveColor: UIColor { TW.green500 }
    override var buyButtonInactiveColor: UIColor { TW.gray800 }
    override var sellButtonActiveColor: UIColor { TW.red600 }
    override var sellButtonInactiveColor: UIColor { TW.gray800 }
    override var buySellTintColor: UIColor { TW.white }
    override var spreadLabelColor: UIColor { TW.gray400 }
    override var spreadLabelFont: UIFont { .systemFont(ofSize: 12) }
    override var spreadValueColor: UIColor { TW.gray400 }
    override var spreadValueFont: UIFont { .systemFont(ofSize: 12) }
    override var settingsLabelColor: UIColor { TW.white }
    override var settingsLabelFont: UIFont { .systemFont(ofSize: 16) }
    override var confirmButtonTitleColor: UIColor { TW.gray400 }
    override var settingsViewBgc: UIColor { TW.gray800 }
    override var moreButtonTintColor: UIColor { TW.gray500 }
    override var spreadViewBorderColor: UIColor { TW.gray800 }
    var priceBoldLabelColor: UIColor { TW.white }
    var priceBoldLabelFont: UIFont { .boldSystemFont(ofSize: 14) }
    var quantitiBoldLabelColor: UIColor { TW.white }
    var quantitiBoldLabelFont: UIFont { .boldSystemFont(ofSize: 14) }
}
