//
//  SpreadVM.swift
//  ForDR
//
//  Created by dan4 on 13.12.2022.
//

import RxCocoa
import RxSwift
import UIKit

struct SpreadViewVM {
    
    var appearance: SpreadAppearancable
    
    // MARK: - Variables
    
    let labelTitle: String
    let valueLabelTitle: String
    
}
