//
//  SpreadView.swift
//  ForDR
//
//  Created by dan4 on 13.12.2022.
//

import Foundation
import RxCocoa
import RxSwift
import SnapKit

/*
 ********************************************************
 DESCRIPTION:
 CREATED FOR:
 ********************************************************
 */

class SpreadView: UIView {
    
    // MARK: - Variables
    
    private let vr = ViewResources()
    var viewModel: SpreadViewVM?
    
    // MARK: - Constructors
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initView()
        initConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /// View Initialization.
    ///
    /// Here the views are registered, their states and location in the
    /// paternal views are set.
    ///
    private func initView() {
        self.backgroundColor = .red
        addSubview(vr.spreadView)
        [vr.spreadLabel, vr.spreadValueLabel].forEach { vr.spreadView.addSubview($0) }
    }
    
    /// Constraints initialization
    ///
    /// Setup constraints for controls are set
    ///
    private func initConstraints() {
        vr.spreadView.snp.makeConstraints {
            $0.edges.equalToSuperview().inset(0)
            $0.height.equalTo(50)
            $0.width.equalTo(59)
        }
        vr.spreadLabel.snp.makeConstraints {
            $0.top.equalToSuperview().inset(9)
            $0.leading.equalToSuperview().inset(8)
        }
        vr.spreadValueLabel.snp.makeConstraints {
            $0.top.equalTo(vr.spreadLabel.snp.bottom).offset(3)
            $0.leading.trailing.equalToSuperview().inset(16)
        }
    }
    
}

// MARK: - Actions

/*
 *********************** Actions ************************
 Handling user actions. For example, clicking on a button,
 changing the segment index
 ********************************************************
 */

extension SpreadView {
}

// MARK: - Configuration

/*
 ******************** Configuration *********************
 All control configuration logic, based on ViewModel.
 Such as: establishing the connection of the view model with
 the state machine, setting the data of all internal components,
 setting the color palette, fonts, pictures.
 ********************************************************
 */

extension SpreadView {

    /// Configuration of control
    ///
    /// Linking the view model with the state machine, appearance
    /// and setting data for inner controls
    ///
    /// - parameter viewModel: ViewModel with necessary data
    ///
    func configure(with viewModel: SpreadViewVM) {
        self.viewModel = viewModel
        setupData(viewModel)
        setupAppearance(viewModel.appearance)
    }
    
    /// Setup data for inner controls
    ///
    /// - parameter viewModel: ViewModel with data for inner controls
    ///
    private func setupData(_ data: SpreadViewVM) {
        vr.spreadLabel.text = data.labelTitle
        vr.spreadValueLabel.text = data.valueLabelTitle
    }
    
    /// Setup appearance for current control
    ///
    /// - parameter appearance: Appearancable class with color, font, image settings
    ///
    private func setupAppearance(_ appearance: SpreadAppearancable) {
        self.backgroundColor = appearance.backgroundColor
        vr.spreadLabel.textColor = appearance.titleColor
        vr.spreadLabel.font = appearance.titleFont
        vr.spreadValueLabel.textColor = appearance.valueLabelColor
        vr.spreadValueLabel.font = appearance.valueLabelFont
    }
    
}

// MARK: - ViewResources

/*
 ******************** View Resources ********************
 All internal components are hidden in resources, for
 readability of the code, and, if necessary, reuse resources in
 other components.
 ********************************************************
 */

private class ViewResources {
    lazy var spreadView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 8
        view.layer.borderWidth = 2
        return view
    }()
    
    lazy var spreadLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()
    
    lazy var spreadValueLabel: UILabel = {
        let label = UILabel()
        return label
    }()
}
