//
//  SpreadAppearance.swift
//  ForDR
//
//  Created by dan4 on 13.12.2022.
//

import RxCocoa
import RxSwift
import UIKit

protocol SpreadAppearancable {
    var backgroundColor: UIColor { get }
    var titleColor: UIColor { get }
    var titleFont: UIFont { get }
    var valueLabelColor : UIColor { get }
    var valueLabelFont: UIFont { get }
    var borderColor: UIColor { get }
}

class SpreadAppearance: SpreadAppearancable {
    var backgroundColor: UIColor { TW.gray900 }
    var titleColor: UIColor { TW.gray400 }
    var titleFont: UIFont { .systemFont(ofSize: 12) }
    var valueLabelColor: UIColor { TW.gray400 }
    var valueLabelFont: UIFont { .systemFont(ofSize: 12) }
    var borderColor: UIColor { TW.gray800 }
}
