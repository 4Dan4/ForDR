//
//  SellButtonDelegate.swift
//  ForDR
//
//  Created by dan4 on 07.12.2022.
//

import Foundation

protocol SellButtonDelegate {
    func sellButtonDelegate(isActive: Bool)
}
