//
//  SellButtonVM.swift
//  ForDR
//
//  Created by dan4 on 06.12.2022.
//

import RxCocoa
import RxSwift
import UIKit

struct SellButtonVM {
    
    var appearance: ButtonAppearancable = SellButtonAppearance()
    
    // MARK: - Variables
    
    let buttonTitle: String
    
}
