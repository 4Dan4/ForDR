//
//  SellButtonView.swift
//  ForDR
//
//  Created by dan4 on 06.12.2022.
//

import Foundation
import RxCocoa
import RxSwift
import SnapKit

/*
 ********************************************************
 DESCRIPTION:
 CREATED FOR:
 ********************************************************
 */

class SellButtonView: UIView {
   
    // MARK: - Variables
    
    private let vr = ViewResources()
    var viewModel: ButtonVM?
    var delegate: ButtonDelegate?
    
    // MARK: - Constructors
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initView()
        initConstraints()
        initActions()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /// View Initialization.
    ///
    /// Here the views are registered, their states and location in the
    /// paternal views are set.
    ///
    private func initView() {
        self.backgroundColor = .red
        addSubview(vr.sellButton)
    }
    
    /// Constraints initialization
    ///
    /// Setup constraints for controls are set
    ///
    private func initConstraints() {
        vr.sellButton.snp.makeConstraints {
            $0.edges.equalToSuperview().inset(0)
        }
    }
    
}

// MARK: - Actions

/*
 *********************** Actions ************************
 Handling user actions. For example, clicking on a button,
 changing the segment index
 ********************************************************
 */

extension SellButtonView {
    
    private func initActions() {
        vr.sellButton.addTarget(self, action: #selector(sellButtonAction), for: .touchUpInside)
    }
    
    @objc
    private func sellButtonAction() {
        self.delegate?.buttonDelegate(isActive: false)
    }
    
}

// MARK: - Configuration

/*
 ******************** Configuration *********************
 All control configuration logic, based on ViewModel.
 Such as: establishing the connection of the view model with
 the state machine, setting the data of all internal components,
 setting the color palette, fonts, pictures.
 ********************************************************
 */

extension SellButtonView {

    /// Configuration of control
    ///
    /// Linking the view model with the state machine, appearance
    /// and setting data for inner controls
    ///
    /// - parameter viewModel: ViewModel with necessary data
    ///
    func configure(with viewModel: ButtonVM) {
        self.viewModel = viewModel
        setupData(viewModel)
        setupAppearance(viewModel.appearance)
    }
    
    /// Setup data for inner controls
    ///
    /// - parameter viewModel: ViewModel with data for inner controls
    ///
    private func setupData(_ data: ButtonVM) {
        vr.sellButton.setTitle(data.buttonTitle, for: .normal)
    }
    
    /// Setup appearance for current control
    ///
    /// - parameter appearance: Appearancable class with color, font, image settings
    ///
    private func setupAppearance(_ appearance: ButtonAppearancable) {
        self.backgroundColor = appearance.backgroundColor
        vr.sellButton.backgroundColor = appearance.inactiveButtonColor
        vr.sellButton.tintColor = appearance.buttonTintColor
    }
    
}

// MARK: - ViewResources

/*
 ******************** View Resources ********************
 All internal components are hidden in resources, for
 readability of the code, and, if necessary, reuse resources in
 other components.
 ********************************************************
 */

private class ViewResources {
    lazy var sellButton: UIButton = {
        let button = UIButton(type: .system)
        button.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
        button.layer.cornerRadius = 8
        return button
    }()
}
