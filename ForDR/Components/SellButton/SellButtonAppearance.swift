//
//  SellButtonAppearance.swift
//  ForDR
//
//  Created by dan4 on 06.12.2022.
//

import RxCocoa
import RxSwift
import UIKit

//protocol SellButtonAppearancable {
//    var backgroundColor: UIColor { get }
//    var sellButtonActiveColor: UIColor { get }
//    var inactiveButtonColor: UIColor { get }
//    var buttonTintColor: UIColor { get }
//}
//
//class SellButtonAppearance: SellButtonAppearancable {
//    var backgroundColor: UIColor { .clear }
//    var sellButtonActiveColor: UIColor { .clear }
//    var inactiveButtonColor: UIColor { .clear }
//    var buttonTintColor: UIColor { TW.white }
//}
