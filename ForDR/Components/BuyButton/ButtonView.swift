//
//  BuyButtonView.swift
//  ForDR
//
//  Created by dan4 on 06.12.2022.
//

import Foundation
import RxCocoa
import RxSwift
import SnapKit

/*
 ********************************************************
 DESCRIPTION:
 CREATED FOR:
 ********************************************************
 */

class ButtonView: UIView {
    
    // MARK: - Variables
    
    private let vr = ViewResources()
    var viewModel: ButtonVM?
    var delegate: ButtonDelegate?
    var buyButtonType: Bool?
    
    // MARK: - Constructors
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initView()
        initConstraints()
        initActions()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /// View Initialization.
    ///
    /// Here the views are registered, their states and location in the
    /// paternal views are set.
    ///
    private func initView() {
        self.backgroundColor = .red
        addSubview(vr.button)
    }
    
    /// Constraints initialization
    ///
    /// Setup constraints for controls are set
    ///
    private func initConstraints() {
        vr.button.snp.makeConstraints {
            $0.edges.equalToSuperview().inset(0)
        }
    }
    
}

// MARK: - Actions

/*
 *********************** Actions ************************
 Handling user actions. For example, clicking on a button,
 changing the segment index
 ********************************************************
 */

extension ButtonView {
    
    private func initActions() {
        vr.button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
    }
    @objc
    private func buttonAction() {
        self.delegate?.buttonDelegate(isActive: buyButtonType ?? false)
    }
}

// MARK: - Configuration

/*
 ******************** Configuration *********************
 All control configuration logic, based on ViewModel.
 Such as: establishing the connection of the view model with
 the state machine, setting the data of all internal components,
 setting the color palette, fonts, pictures.
 ********************************************************
 */

extension ButtonView {

    /// Configuration of control
    ///
    /// Linking the view model with the state machine, appearance
    /// and setting data for inner controls
    ///
    /// - parameter viewModel: ViewModel with necessary data
    ///
    func configure(with viewModel: ButtonVM) {
        self.viewModel = viewModel
        setupData(viewModel)
    }
    
    /// Setup data for inner controls
    ///
    /// - parameter viewModel: ViewModel with data for inner controls
    ///
    private func setupData(_ data: ButtonVM) {
        if data.buttonType == .buyButton {
            setupAppearanceForBuyButton(data.appearance)
            vr.button.setTitle(data.buttonTitle, for: .normal)
            vr.button.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 12.0, right: 125.0)
            vr.button.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
            buyButtonType = true
        } else if data.buttonType == .sellButton {
            setupAppearanceForSellButton(data.appearance)
            vr.button.setTitle(data.buttonTitle, for: .normal)
            vr.button.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: 125.0, bottom: 12.0, right: 0.0)
            vr.button.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
            buyButtonType = false
        }
    }
    
    /// Setup appearance for current control
    ///
    /// - parameter appearance: Appearancable class with color, font, image settings
    ///
    private func setupAppearanceForBuyButton(_ appearance: ButtonAppearancable) {
        self.backgroundColor = appearance.backgroundColor
        vr.button.backgroundColor = appearance.buttonActiveColor
        vr.button.tintColor = appearance.buttonTintColor
    }
    
    private func setupAppearanceForSellButton(_ appearance: ButtonAppearancable) {
        self.backgroundColor = appearance.backgroundColor
        vr.button.backgroundColor = appearance.buttonActiveColor
        vr.button.tintColor = appearance.buttonTintColor
    }
    
}

// MARK: - ViewResources

/*
 ******************** View Resources ********************
 All internal components are hidden in resources, for
 readability of the code, and, if necessary, reuse resources in
 other components.
 ********************************************************
 */

private class ViewResources {
    lazy var button: UIButton = {
        let button = UIButton(type: .system)
        button.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
        button.layer.cornerRadius = 8
        return button
    }()
}
