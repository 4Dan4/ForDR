//
//  BuyButtonVM.swift
//  ForDR
//
//  Created by dan4 on 06.12.2022.
//

import RxCocoa
import RxSwift
import UIKit

struct ButtonVM {
    
    var appearance: ButtonAppearancable
    
    // MARK: - Variables
    
    let buttonTitle: String
    let buttonType: ButtonType
    
}
