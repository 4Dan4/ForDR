//
//  BuyButtonDelegate.swift
//  ForDR
//
//  Created by dan4 on 07.12.2022.
//

import Foundation

protocol ButtonDelegate {
    func buttonDelegate(isActive: Bool)
}
