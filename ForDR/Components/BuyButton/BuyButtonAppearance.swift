//
//  BuyButtonAppearance.swift
//  ForDR
//
//  Created by dan4 on 06.12.2022.
//

import RxCocoa
import RxSwift
import UIKit

protocol ButtonAppearancable {
    var backgroundColor: UIColor { get }
    var buttonActiveColor: UIColor { get }
    var inactiveButtonColor: UIColor { get }
    var buttonTintColor: UIColor { get }
}

class BuyButtonAppearance: ButtonAppearancable {
    var backgroundColor: UIColor { .clear }
    var buttonActiveColor: UIColor { .clear }
    var inactiveButtonColor: UIColor { .clear }
    var buttonTintColor: UIColor { TW.white }
}

class SellButtonAppearance: ButtonAppearancable {
    var backgroundColor: UIColor { .clear }
    var buttonActiveColor: UIColor { .clear }
    var inactiveButtonColor: UIColor { .clear }
    var buttonTintColor: UIColor { TW.white }
}
