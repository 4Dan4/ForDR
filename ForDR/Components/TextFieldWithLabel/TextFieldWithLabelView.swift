//
//  TextFieldWithLabelView.swift
//  ForDR
//
//  Created by dan4 on 06.12.2022.
//

import Foundation
import RxCocoa
import RxSwift
import SnapKit

/*
 ********************************************************
 DESCRIPTION:
 CREATED FOR:
 ********************************************************
 */

class TextfieldWithTitleView: UIView {
   
    // MARK: - Variables
    
    private let vr = ViewResources()
    var viewModel: TextFieldWitTitleVM?
    
    // MARK: - Constructors
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initView()
        initConstraints()
        initActions()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /// View Initialization.
    ///
    /// Here the views are registered, their states and location in the
    /// paternal views are set.
    ///
    private func initView() {
        [vr.titleLabel, vr.stackView].forEach { addSubview($0) }
        [vr.textField, vr.actionButton].forEach { vr.stackView.addArrangedSubview($0) }
    }
    
    /// Constraints initialization
    ///
    /// Setup constraints for controls are set
    ///
    private func initConstraints() {
        vr.titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().inset(0)
            $0.leading.equalToSuperview().inset(0)
        }
        vr.stackView.snp.makeConstraints {
            $0.top.equalTo(vr.titleLabel.snp.bottom).offset(8)
            $0.leading.trailing.equalToSuperview().inset(0)
            $0.height.equalTo(52)
        }
        vr.textField.snp.makeConstraints {
            $0.top.equalTo(vr.titleLabel.snp.bottom).offset(8)
            $0.leading.equalToSuperview().inset(0)
            $0.height.equalTo(52)
        }
        vr.actionButton.snp.makeConstraints {
            $0.top.equalTo(vr.textField.snp.top).offset(25)
            $0.height.width.equalTo(52)
            $0.trailing.equalToSuperview().inset(0)
        }
    }
    
}

// MARK: - Actions

/*
 *********************** Actions ************************
 Handling user actions. For example, clicking on a button,
 changing the segment index
 ********************************************************
 */

extension TextfieldWithTitleView {
    
    private func initActions() {
        vr.fieldClearButton.addTarget(self, action: #selector(clearButtonAction), for: .touchUpInside)
    }
    
    @objc
    private func clearButtonAction() {
        vr.textField.text = ""
        vr.textField.endEditing(true)
    }
    
}

// MARK: - Configuration

/*
 ******************** Configuration *********************
 All control configuration logic, based on ViewModel.
 Such as: establishing the connection of the view model with
 the state machine, setting the data of all internal components,
 setting the color palette, fonts, pictures.
 ********************************************************
 */

extension TextfieldWithTitleView {

    /// Configuration of control
    ///
    /// Linking the view model with the state machine, appearance
    /// and setting data for inner controls
    ///
    /// - parameter viewModel: ViewModel with necessary data
    ///
    func configure(with viewModel: TextFieldWitTitleVM) {
        self.viewModel = viewModel
        setupData(viewModel)
        setupAppearance(viewModel.appearance)
    }
    
    /// Setup data for inner controls
    ///
    /// - parameter viewModel: ViewModel with data for inner controls
    ///
    private func setupData(_ data: TextFieldWitTitleVM) {
        vr.titleLabel.text = data.titleLabel
        vr.textField.placeholder = data.textFieldPlaceHolder
        if data.clearButtonWithTtitle == true {
            vr.fieldClearButton.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            vr.fieldClearButton.setTitle("Market price  ", for: .normal)
            vr.fieldClearButton.titleLabel?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            vr.fieldClearButton.setImage(UIImage(named: "clear"), for: .normal)
            vr.actionButton.setTitle("⚙️", for: .normal)
        } else if data.clearButtonWithTtitle == false {
            vr.fieldClearButton.setImage(UIImage(named: "clear"), for: .normal)
            vr.actionButton.setTitle("max", for: .normal)
        }
    }
    
    /// Setup appearance for current control
    ///
    /// - parameter appearance: Appearancable class with color, font, image settings
    ///
    private func setupAppearance(_ appearance: TextFieldWithTitleAppearancable) {
        self.backgroundColor = appearance.backgroundColor
        vr.titleLabel.textColor = appearance.titleColor
        vr.titleLabel.font = appearance.titleFont
        vr.textField.backgroundColor = appearance.textFieldBgcColor
        vr.textField.layer.borderColor = appearance.textFieldBorderColor.cgColor
        vr.textField.tintColor = appearance.textFieldTintColor
        vr.textField.textColor = appearance.textFieldTextColor
        vr.fieldClearButton.tintColor = appearance.clearButtonTintColor
        vr.actionButton.backgroundColor = appearance.actionButtonBgc
        vr.actionButton.tintColor = appearance.actionButtonTintColor
    }
    
}

// MARK: - ViewResources

/*
 ******************** View Resources ********************
 All internal components are hidden in resources, for
 readability of the code, and, if necessary, reuse resources in
 other components.
 ********************************************************
 */

private class ViewResources {
    lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.spacing = 17
        return stackView
    }()
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        return label
    }()
    
    lazy var textField: TextFieldWithPadding = {
        let textField = TextFieldWithPadding()
        textField.keyboardType = .asciiCapableNumberPad
        textField.attributedPlaceholder = NSAttributedString(string: "0", attributes: [NSAttributedString.Key.foregroundColor: TW.gray500])
        textField.borderStyle = .roundedRect
        textField.layer.cornerRadius = 8
        textField.layer.borderWidth = 1
        textField.rightView = fieldClearButton
        textField.rightViewMode = .whileEditing
        return textField
    }()
    
    lazy var fieldClearButton: UIButton = {
        let button = UIButton(type: .system)
        return button
    }()
    
    lazy var actionButton: UIButton = {
        let button = UIButton(type: .system)
        button.layer.cornerRadius = 8
        return button
    }()
}
