//
//  TextFieldWithLabelAppearance.swift
//  ForDR
//
//  Created by dan4 on 06.12.2022.
//

import RxCocoa
import RxSwift
import UIKit

protocol TextFieldWithTitleAppearancable {
    var backgroundColor: UIColor { get }
    var titleColor: UIColor { get }
    var titleFont: UIFont { get }
    var textFieldBgcColor: UIColor { get }
    var textFieldBorderColor: UIColor { get }
    var textFieldTintColor: UIColor { get }
    var textFieldTextColor: UIColor { get }
    var clearButtonTintColor: UIColor { get }
    var actionButtonBgc: UIColor { get }
    var actionButtonTintColor: UIColor { get }
}

class TextFieldWithTitleAppearance: TextFieldWithTitleAppearancable {
    var backgroundColor: UIColor { .clear }
    var titleColor: UIColor { TW.white }
    var titleFont: UIFont { .systemFont(ofSize: 14) }
    var textFieldBgcColor: UIColor { TW.black }
    var textFieldBorderColor: UIColor { TW.gray700 }
    var textFieldTintColor: UIColor { TW.white }
    var textFieldTextColor: UIColor { TW.white }
    var clearButtonTintColor: UIColor { TW.gray600 }
    var actionButtonBgc: UIColor { TW.gray800 }
    var actionButtonTintColor: UIColor { TW.gray500 }
}
