//
//  TextFieldWithLabelVM.swift
//  ForDR
//
//  Created by dan4 on 06.12.2022.
//

import Foundation

struct TextFieldWitTitleVM {
    
    var appearance: TextFieldWithTitleAppearancable = TextFieldWithTitleAppearance()
    
    var onInfoClick: (() -> Void)?
    
    // MARK: - Variables
    
    let titleLabel: String
    let textFieldPlaceHolder: String
    let clearButtonWithTtitle: Bool
    
}
